<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRelationFromPaymentsToPrivateRoutes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('private_route_keys', function (Blueprint $table) {
            $table->unsignedInteger('payment_id')->after('used');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('private_route_keys', function (Blueprint $table) {
           $table->dropColumn('payment_id');
        });
    }
}
