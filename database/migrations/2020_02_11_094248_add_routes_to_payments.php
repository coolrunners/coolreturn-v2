<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRoutesToPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payments', function(Blueprint $table) {
            $table->float('amount')->after('order_id')->nullable();
            $table->string('payment_url')->after('package_number')->nullable();
            $table->string('process_url')->after('package_number')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payments', function(Blueprint $table) {
            $table->dropColumn('amount');
            $table->dropColumn('payment_url');
            $table->dropColumn('process_url');

        });
    }
}
