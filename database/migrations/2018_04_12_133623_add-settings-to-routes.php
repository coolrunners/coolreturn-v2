<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSettingsToRoutes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('route_settings', function (Blueprint $table) {
            $table->integer('routes_id')->unsigned()->length(10);
            $table->longText('css_style')->nullable();
            $table->mediumText('settings')->nullable();
            $table->timestamps();
        });

        Schema::table('route_settings', function(Blueprint $table) {
            $table->foreign('routes_id')->references('id')->on('routes')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('route_settings');
    }
}
