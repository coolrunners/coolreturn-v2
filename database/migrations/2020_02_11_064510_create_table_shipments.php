<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableShipments extends Migration
{

    public function up()
    {
        Schema::create('shipments', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedInteger('route_id');
            $table->unsignedInteger('payment_id')->nullable();

            $table->string('package_number')->nullable()->unique();
            $table->string('labelless_code')->nullable();


            $table->string("sender_name")->nullable();
            $table->string("sender_attention")->nullable();
            $table->string("sender_street1")->nullable();
            $table->string("sender_zip_code")->nullable();
            $table->string("sender_city")->nullable();
            $table->string("sender_country")->nullable();
            $table->string("sender_phone")->nullable();
            $table->string("sender_mail")->nullable();

            $table->string("receiver_name")->nullable();
            $table->string("receiver_attention")->nullable();
            $table->string("receiver_street1")->nullable();
            $table->string("receiver_zip_code")->nullable();
            $table->string("receiver_city")->nullable();
            $table->string("receiver_country")->nullable();
            $table->string("receiver_phone")->nullable();
            $table->string("receiver_mail")->nullable();

            $table->string('carrier');
            $table->string('carrier_product')->nullable();
            $table->string('carrier_service')->nullable();

            $table->string('reference')->nullable();

            $table->float('price')->nullable();

            $table->foreign('route_id')->references('id')->on('routes');
            $table->foreign('payment_id')->references('id')->on('payments');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipments');

    }
}
