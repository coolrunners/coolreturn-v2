<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePrivateRoutes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('private_route_keys', function (Blueprint $table) {
            $table->dropColumn('payment_id');
            $table->unsignedBigInteger('shipment_id')->nullable();
            $table->foreign('shipment_id')->references('id')->on('shipments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('private_route_keys', function (Blueprint $table) {
            $table->dropForeign('private_route_keys_shipment_id_foreign');
            $table->dropColumn('shipment_id');
            $table->unsignedInteger('payment_id')->after('used');
        });
    }
}
