<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrivateRouteKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('private_route_keys', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('routes_id')->unsigned()->length(10);
            $table->string('key');
            $table->integer('used')->default(0);
            $table->timestamps();
        });

        Schema::table('private_route_keys', function(Blueprint $table) {
            $table->foreign('routes_id')->references('id')->on('routes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
