<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_id');
            $table->unsignedInteger('route_id');
            $table->string('status')->default(0)->comment('0: pending, 1: captured, 99: canceled');
            $table->string('transaction_id')->nullable();
            $table->string('package_number')->nullable();
            $table->timestamps();
        });

        Schema::table('payments', function(Blueprint $table) {
           $table->foreign('route_id')
            ->references('id')->on('routes')
            ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
