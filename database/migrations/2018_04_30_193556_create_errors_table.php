<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateErrorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('errors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('unique_id');
            $table->unsignedInteger('order_id');
            $table->string('exception');
            $table->json('data')->nullable();
            $table->timestamps();
        });

        Schema::table('errors', function(Blueprint $table) {
            $table->foreign('order_id')
                  ->references('id')->on('payments')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('errors');
    }
}
