<?php

return [
    "lang" => "en-GB",
    "price" =>  'Buy Label <br> :price ,- DKK.',
    'Name' =>  "Name",
    'Street' =>  "Address",
    "Zip_code" =>  "Zipcode",
    "City" =>  "City",
    "Phone" =>  "Mobile",
    "Terms_conditions" =>  'I accept the <a target="_blank" href="http://coolrunner.dk/terms-and-conditions ">Terms and Conditions</a>',
    "Package_size" =>  "I confirm that the longest side of the package is less than 90 cm and that the maximum weight of the package is 20 kg. Furthermore, I confirm that I have access to print my return label on an A4 printer.",
    "Email" => "Email",
    "field" => "The :field field is required.",
    "Att" => "att",
    "Order_number" => "Order Number",
    "subheader" => "Your Information",
    "freeLabel" => "Get Label",
];
