<?php

return [
    "header" => "Your order has been placed!",
    "ordernumber" => "Your order number is: <u>:order</u>",
    "order" => "Coolrunner Return Label",
    "instructions" => "We have sent an order confirmation to your email.<br><br>Your label will begin downloading shortly, <br>if not then click ",
    "here" => "here.",
    "labelless_code" => "<br>Du kan også bruge din labelless kode: <u>:order</u>"

];