<?php

return [
    "lang" => "se-SE",
    "price" => 'Köp etiket <br> :price ,- DKK.',
    'Name' => "Namn",
    'Street' => "Adress",
    "Zip_code" => "Postnummer",
    "City" => "Ort",
    "Phone" => "Telefonnummer",
    "Terms_conditions" => 'Jag godkänner <a target="_blank" href="http://coolrunner.dk/terms-and-conditions">villkoren</a>',
    "Package_size" => "Jag bekräftar att paketets längsta sida är kortare än 90 cm och att paketet väger max 20 kg. Dessutom bekräftar jag att jag har möjlighet att skriva ut min returetikett på en-GB A4-skrivare.",
    "Email" => "Mail",
    "field" => ":field missing.",
    "Att" => "att",
    "Order_number" => "Ordernummer",
    "subheader" => "Your Information",
    "freeLabel" => "Get Label",
];
