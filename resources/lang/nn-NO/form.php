<?php

return [
    "lang" => "nn-NO",
    "price" => 'Kjøp seddel <br> :price ,- DKK.',
    'Name' => "Navn",
    'Street' => "Adresse",
    "Zip_code" => "Postnummer",
    "City" => "By",
    "Phone" => "Telefonnummer",
    "Terms_conditions" => 'Jeg aksepterer disse <a target="_blank" href="http://coolrunner.dk/terms-and-conditions">vilkår og betingelsene</a>',
    "Package_size" => "Jeg bekrefter at den lengste siden av pakken er mindre enn 90 cm, og at pakkens maksimumvekt er 20 kg. Videre bekrefter jeg at jeg har tilgang til å skrive ut min returseddel på en-GB A4 printer. ",
    "Email" => "E-post",
    "field" => ":field missing.",
    "Att" => "att",
    "Order_number" => "Ordrenummer",
    "subheader" => "Your Information",
    "freeLabel" => "Get Label",
];
