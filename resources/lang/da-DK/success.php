<?php

return [
    "header" => "Tak for din returordre!",
    "ordernumber" => "Dit ordre-nummer er <u>:order</u>",
    "order" => "Coolrunner Return Label",
    "instructions" => "Vi har sendt dig en-GB mail med alle dine ordreinformationer.<br><br>Din label vil blive downloadet automatisk. <br>Du har også mulighed for at downloade den ",
    "here" => "her",
    "labelless_code" => "<br><strong>Send din pakke uden at printe en-GB pakkelabel</strong> <br> <ol> <li>Skriv den unikke pakkekode tydeligt på pakken.</li> <li>Aflevér pakken på et af DAO's <a href='https://coolrunner.dk/indleveringssteder'  target='_blank' >indleveringssteder</a>.</li> </ol> <strong>Pakkekode</strong>: <u>:order</u>"

];
