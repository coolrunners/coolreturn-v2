<?php

return [
    "lang" => "da-DK",
    "price" => 'Køb Label <br> :price ,- DKK.',
    'Name' => "Navn",
    'Street' => "Adresse",
    "Zip_code" => "Postnummer",
    "City" => "By",
    "Phone" => "Tlf.nr.",
    "Terms_conditions" => 'Jeg har læst og bekræfter <a target="_blank" href="http://coolrunner.dk/handelsbetingelser">Handelsbetingelserne</a>',
    "Package_size" => "Jeg bekræfter, at pakkens længste side er mindre end 90 cm og at pakkens vægt er maks. 20 kg. Jeg bekræfter også, at jeg har adgang til at udskrive min returlabel på en-GB A4-printer.",
    "Email" => "Email",
    "field" => ":field mangler.",
    "Att" => "att",
    "Order_number" => "Ordrenummer",
    "subheader" => "Din information",
    "freeLabel" => "Hent Label"
];
