<?php
$fields =  [
    "name" => ["name" => "name", "type" => "text"],
    "street" => ["name" => "street", "type" => "text"],
    "zipcode_city" => [
        "type" => "multi.text",
        "fields" => [
            "zip_code" => ["name" => "zip_code", "type" => "number"],
            "city" => ["name" => "city", "type" => "text"],
        ],
    ],
    "country" => ["name" => "country", "type" => "select"],
    "phone" => ["name" => "phone", "type" => "number"],
    "email" => ["name" => "email", "type" => "text"],
    "terms_conditions" => ["name" => "terms_conditions", "type" => "checkbox"],
    "package_size" => ["name" => "package_size", "type" => "checkbox"],
];

$disabled_route = in_array($data->zone, []);
?>
@if($disabled_route)
    <div id="overlay">
        <h1>Due to COVID-19, Spain has decided to close all servicepoints.</h1>
    </div>
@endif

<form method="POST" action="{!! url("request") !!}" <?= $disabled_route ? 'class="disabled"' : ''; ?>>
    @csrf


    <div class="subheader">
        <h2>{!! __('form.subheader') !!}</h2>
    </div>
    @each('return.field',$fields, 'field')

    @include('return.settings')

    @if(!$disabled_route)
        <button id="submit" type="submit">{!! $data->price > 0 ? (__('form.price', array('price' => $data->price)) ?? 0) : __('form.freeLabel') !!}
            @if($data->zone != 'DK' && (int)$data->price > 0)
                <p style="font-size: xx-small;">{{$data->price}} ,- DKK estimates to ~ {!! round($data->price * 0.134324,2)  !!} ,- EUR</p>
            @endif
        </button>
    @endif

</form>

<style>
    .subheader {
        float: left;
        width: 100%;
        padding-bottom:  1em;
    }
    form.disabled input {
        pointer-events:none;
    }

    #overlay {
        position: fixed; /* Sit on top of the page content */
        display: block; /* Hidden by default */
        width: 100%; /* Full width (cover the whole page) */
        height: 100%; /* Full height (cover the whole page) */
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: rgba(0,0,0,0.5); /* Black background with opacity */
        z-index: 2; /* Specify a stack order in case you're using a different order for other elements */
        cursor: pointer; /* Add a pointer on hover */
    }

    #overlay h1{
        position: absolute;
        top: 25%;
        left: 50%;
        font-size: 50px;
        color: white;
        transform: translate(-50%,-50%);
        -ms-transform: translate(-50%,-50%);
        text-align: center;
        background-color:#3e3e3e;
        padding: 0.5em;
    }
</style>
