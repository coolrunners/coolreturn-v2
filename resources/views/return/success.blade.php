<!doctype html>
<html>
<head>
    <title>Your order {{$order->order_id}} is ready</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <meta name="robots" content="noindex, nofollow">
    <meta name="googlebot" content="noindex, nofollow, noarchive">
    <link rel="stylesheet" href="{{ URL::asset('css/success.css') }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
</head>

<body>

<div class="coolrunner">
    <div class="coolrunner__header">
        <div class="coolrunner__logo-wrapper">
            <img src="{{ URL::asset('svg/coolrunner-logo.svg') }}" class="coolrunner__logo" alt="Coolrunner">
        </div>
        <div class="coolrunner__header-info">
            <span class="coolrunner__date">{{$order->created_at->format('d.m.Y')}}</span>
            <span class="coolrunner__ref">{{$order->order_id}}</span>
        </div>
    </div>

    <div class="coolrunner__subheader-wrapper">
        <div class="coolrunner__subheader">
            <h1 class="coolrunner__username">{!! __('success.header') !!}</h1>
            <span class="coolrunner__help-text">{!! __('success.ordernumber',["order" =>$order->shipment->package_number]) !!} </span>


        </div>
    </div>

    <div class="coolrunner__cart">

        <ul class="coolrunner__cart-list">
            <li class="coolrunner__cart-item">
                <span class="coolrunner__index"></span>
                <span class="coolrunner__item-name">{!! __("success.order") !!}</span>
                <span class="coolrunner__item-price">{{number_format($order->route->price,2)}} DKK,-</span>
            </li>

        </ul>
    </div>

    <div class="coolrunner__footer">


        <span class="coolrunner__help-text">{!! __("success.instructions") !!} <a data-auto-download
                                                                       href="{{url('/request/pdf/'.$order->shipment->slug())}}"> {!! __("success.here") !!}</a>.<br>
        </span>
        <?php if(($order->shipment->labelless_code ?? null)) : ?>
        <span class="coolrunner__help-text">{!! __('success.labelless_code',["order" =>$order->shipment->labelless_code ?? null]) !!} </span>
        <?php endif; ?>
    </div>
</div>
<span class="delivered-by">
      <a target="_blank" href="https://coolrunner.dk"><img src="{{ URL::asset('svg/logo-clear.svg') }}" alt="Coolrunner">
          <span>Delivered by CoolRunner</span>
      </a>
    </span>

<script>

    jQuery(function () {
        jQuery('a[data-auto-download]').each(function () {
            var $this = $(this);
            setTimeout(function () {
                window.location = $this.attr('href');
            }, 5000);
        });
    });

</script>
</body>
</html>

