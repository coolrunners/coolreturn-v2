<script>

    jQuery(function() {
        jQuery('a[data-auto-download]').each(function(){
            var $this = $(this);
            setTimeout(function() {
                window.location = $this.attr('href');
            }, 2000);
        });
    });

</script>
