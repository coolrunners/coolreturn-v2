
<?php
$data= session('view');
$locale= session('locale');
?>
@switch($field['type'])
    @case('text')
    <div class="form-group">
        <input type="text" class="form-control {!! $errors->has($field['name']) ? 'error' : ''!!}" id="{{$field['name']}}" name="{{$field['name']}}"
               placeholder="{!! $errors->has($field['name']) ? __('form.field',["field" => __("form.".ucfirst($field['name'])) ]): __("form.".ucfirst($field['name'])) !!}"
               value="{{ old($field['name']) }}" maxlength="35">
    </div>

    @break

    @case('multi.text')

    @foreach($field['fields'] as $_field)
        <div class="form-group {{$_field['name']}} clearfix">
            <input type="text" class="form-control {!! $errors->has($_field['name']) ? 'error' : ''!!}" id="{{$_field['name']}}" name="{{$_field['name']}}"
                   placeholder="{!! $errors->has($_field['name']) ? __('form.field',["field" => __("form.".ucfirst($_field['name'])) ]): __("form.".ucfirst($_field['name'])) !!}"

                   value="{{ old($_field['name']) }}">

        </div>
    @endforeach
    @break


    @case('checkbox')
    <div class="form-group">
        <div class="checkbox {!! $errors->has($field['name']) ? 'error' :''!!}">
            <input type="checkbox" value="0" name="{{$field['name']}}" id="{{$field['name']}}">
            <label for="{{$field['name']}}"><span>{!!__("form.".ucfirst($field['name'])) !!}</span></label>
        </div>
        @if ($errors->has($field['name']))

        @endif
    </div>
    @break

    @case('number')
    <div class="form-group">
        <input type="number" pattern="\d*" class="form-control {!! $errors->has($field['name']) ? 'error' :''!!}" id="{{$field['name']}}" name="{{$field['name']}}"
               placeholder="{!! $errors->has($field['name']) ? __('form.field',["field" => __("form.".ucfirst($field['name'])) ]) : __("form.".ucfirst($field['name'])) !!}"
               value="{{ old($field['name']) }}">
    </div>
    @break

    @case('select')
    <div class="form-group">
        <select readonly="" id="{{$field['name']}}" name="{{$field['name']}}" disabled>
            <option selected value="{{$data->zone}}">{!! json_decode(Countries::lookup($locale))->{$data->zone} !!}</option>
        </select>    </div>
    @break

@endswitch

