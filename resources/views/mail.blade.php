<!--@subject E-mail - Header @-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="initial-scale=1.0, width=device-width" />
</head>
<body>
<style>
    *, *:before, *:after {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        margin: 0;
        padding: 0;
    }

    html, body {
        height: 100%;
    }

    html, body, img, fieldset, abbr, acronym {
        border: 0;
    }

    body {
        font-family: sans-serif;
        margin: 0;
        border: 0;
        padding: 0;
        background: #F6F6F6;
        font-size: 14px;
        line-height: 18px;
    }

    p {
        margin-bottom: 10px;
    }

    img {
        max-width: 100%;
    }

    .wrapper {
        width: auto;
        max-width: 640px;
        margin: 0 auto;
        border: 2px solid #ECECEC;
        border-radius: 5px;
        padding: 15px 25px 25px;
        background-color: #fff;
        margin-top: 20px;
    }

    table tr td img {
        margin-bottom: 20px;
        max-height: 200px;
    }
    table tr td h1 {
        margin-bottom: 15px;
    }
</style>

<div class="wrapper">

    <?php if(View::exists("mails.order-{$zone}")): ?>
        @include("mails.order-{$zone}")

    <?php else : ?>
        @include("mails.order-EN")
    <?php endif; ?>


</div>

</body>