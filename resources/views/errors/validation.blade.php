<?php \Illuminate\Support\Facades\Session::flush()?>
        <!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Validation error</title>

    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
    <link rel='stylesheet prefetch' href='https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css'>
    <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>

    <link rel="stylesheet"
          href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/default.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>
    <script>hljs.initHighlightingOnLoad();</script>

    <style>
        body {
            margin: 0;
            overflow: hidden;
        }

        .error {
            box-sizing: border-box;
            width: 100vw;
            height: 100vh;
        }

        .error .center {
            text-align: center;
            position: absolute;
            top: 50%;
            left: 0;
            right: 0;
            transform: translate(0, -50%);
        }

        .error .center h2 {
            margin: 5px 0;
        }

        .error .center a:not(.mail) {
            text-transform: uppercase;
        }

        .error p {
            margin-bottom: 20px;
        }

        .error i {
            font-size: 5em;
            color: #F6655F;
        }

        .error .btn {
            box-shadow: 3px 3px 10px rgba(0, 0, 0, .3);
            margin: 10px;
        }

        .error .btn.btn-default {
            color: #398439;
            border-color: #255625;
        }
    </style>


</head>

<body>

<div class="error">

    <div class="center">
        <i class="la la-times-circle"></i>
        <h2>Sorry</h2>
        <p class="message">
        @if ($errors->has('message'))
        {{ $errors->first('message') }}
            @endif
        </p>
        <div>

        </div>
    </div>
</div>
{{-- for debugging purposes--}}


</body>

</html>
