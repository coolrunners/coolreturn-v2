<!doctype html>
<html>
<head>
    <link rel="stylesheet" href="{{ URL::asset('css/concat.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/errors.css') }}">

</head>
<body>
<div class="page">
    <div class="main-container">

        <h1>404</h1>

        <p>Vi kunne ikke finde siden</p>
    </div>
</div>
</body>