<!doctype html>
<html>
<head>
    <title>
        {{$data->title}}
    </title>
    <link rel="stylesheet" href="{{ URL::asset('css/concat.css') }}">
     @if(isset($data->settings))
        <style type="text/css">{!! preg_replace('/\v(?:[\v\h]+)/', '', $data->settings->css_style) !!}</style>
    @endif
</head>
<body>
<?php  session()->put('view',$data); ?>
<?php  session()->put('locale',$locale);?>




@if ($errors->has('message'))
    <div class="errors">
    <div class="error-messages"> {{ $errors->first('message') }}</div>
    </div>
@endif

<div class="page">
    <div class="main-container clearfix">

            <div class="boxed clearfix overall">
                <div class="description" style="float: left; width: 70%;">
                    <h1>{{$data->title}}</h1>
                    <p>{!!  str_replace(array('{PRICE}', '{RECEIVER_NAME}'), array(str_replace(".",",",$data->price),$data->receiver->company), str_replace("\n", "</p><p>", $data->description)); !!}</p>
                </div>

                <div class="boxed coolreturn-receiver"
                     style="float: right; width: 29%; top: -52px;right: -52px; text-align: center;">
                    @if (!empty($data->logo) AND !empty($data->url))
                        <div class="receiver-logo">
                            <a target="_blank" href="{{$data->url}}">
                                <img src="{{$data->logo}}">
                            </a>
                        </div>
                    @elseif(!empty($data->logo))
                        <img src="{{$data->logo}}">
                    @endif
                        <div class="receiver-name">{{$data->receiver->company}}</div>
                        {{--<div class="receiver-attention">{{$data->receiver->attention}}</div>--}}
                        <div class="receiver-street1">{{$data->receiver->street}}</div>
                        <div class="receiver-zipcode-city">{{$data->receiver->postcode}} {{$data->receiver->city}}</div>
                        <div class="receiver-country">{!! json_decode(Countries::lookup($locale))->{$data->receiver->country_id} !!}</div>
                        <div class="receiver-phone">Tlf: {!!  chunk_split(str_replace(" ", "", $data->receiver->telephone),2) !!}</div>
                        <div class="receiver-email">{{$data->receiver->notification_email}}</div>
                </div>

                @include('return.form')

            </div>
    </div>
</div>
</body>
</html>