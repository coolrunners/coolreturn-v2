<?php 
$price_excl_tax = $price*0.80;
$price_incl_tax = $price;
?>

<table cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td>
            <a href="{{$website}}">
                <img src="{{$logo}}" />
            </a>
        </td>
    </tr>
    <tr>
        <td>
            <h1>Hei {{$sender_name}}</h1>
            <p>Takk for at du opprettet returetiketten ({{$reference}}) gjennom returportalen</p>
            <p>Har du ikke fått lastet ned etiketten? Den er også lagt ved i denne e-posten.</p>
            <p>Ditt pakkenummer er {{$package_number}}, og etter at du har sendt pakken, kan du følge pakkens vei til {{$receiver_name}} på <a href="http://coolrunner.dk?trackandtrace={{$package_number}}">Coolrunner.dk</a></p>
        </td>
    </tr>
    <tr>
        <table width="100%" cellpadding="0" cellspacing="0" border="0">
            <thead>
              <th align="left">Netto beløp</th>
              <th align="left">Moms</th>
              <th align="left">Total beløp</th>
            </thead>
            <tbody>
              <tr>
                <td>
                  {{$price_excl_tax}} kr.
                </td>
                <td>
                  {{$price_incl_tax-$price_excl_tax}} kr.
                </td>
                <td>
                  {{$price_incl_tax}} kr.
                </td>
              </tr>
            </tbody>
        </table>
    </tr>
    <tr>
        <td>
            <h5 class="closing-text" style="margin-top: 10px;">Beste hilsen<br>{{$receiver_name}}</h5>
        </td>
    </tr>
</table>