<?php 
$price_excl_tax = $price*0.80;
$price_incl_tax = $price;
?>

<table cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td>
            <a href="{{$website}}">
                <img src="{{$logo}}" />
            </a>
        </td>
    </tr>
    <tr>
        <td>
            <h1>Hej {{$sender_name}}</h1>
            <p>Tack för att du registrerade din retursedel ({{$reference}}) i vår returportal.</p>
            <p>Om du inte har laddat ned din retursedel finns den även bifogad detta mejl.</p>
            <p>Ditt kollinummer är: {{$package_number}} Efter att du har skickat paketet kan du följa det hela vägen till {{$receiver_name}} på <a href="http://coolrunner.dk?trackandtrace={{$package_number}}">Coolrunner.dk</a></p>
        </td>
    </tr>
    <tr>
        <table width="100%" cellpadding="0" cellspacing="0" border="0">
            <thead>
              <th align="left">Netto mängd</th>
              <th align="left">Moms</th>
              <th align="left">Total mängd</th>
            </thead>
            <tbody>
              <tr>
                <td>
                  {{$price_excl_tax}} kr.
                </td>
                <td>
                  {{$price_incl_tax-$price_excl_tax}} kr.
                </td>
                <td>
                  {{$price_incl_tax}} kr.
                </td>
              </tr>
            </tbody>
        </table>
    </tr>
    <tr>
        <td>
            <h5 class="closing-text">Bästa hälsningar<br>{{$receiver_name}}</h5>
        </td>
    </tr>
</table>