<?php 
$price_excl_tax = $price*0.80;
$price_incl_tax = $price;
?>

<table cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td>
            <a href="{{$website}}">
                <img src="{{$logo}}" />
            </a>
        </td>
    </tr>
    <tr>
        <td>
            <h1>Hej {{$sender_name}}</h1>
            <p>Tak fordi du oprettede din returlabel ({{$reference}}) igennem vores returportal</p>
            <p>Hvis du ikke har fået downloadet din label, er den også vedhæftet i denne mail.</p>
            <p>Dit pakkenummer er: {{$package_number}} og du vil efter du har sendt pakken, kunne følge dens vej til {{$receiver_name}} på <a href="http://coolrunner.dk?trackandtrace={{$package_number}}">Coolrunner.dk</a></p>
        </td>
    </tr>
    <tr>
        <table width="100%" cellpadding="0" cellspacing="0" border="0">
            <thead>
              <th align="left">Netto beløb.</th>
              <th align="left">Moms</th>
              <th align="left">Total beløb</th>
            </thead>
            <tbody>
              <tr>
                <td>
                  {{$price_excl_tax}} kr.
                </td>
                <td>
                  {{$price_incl_tax-$price_excl_tax}} kr.
                </td>
                <td>
                  {{$price_incl_tax}} kr.
                </td>
              </tr>
            </tbody>
        </table>
    </tr>
    <tr>
        <td>
            <h5 class="closing-text" style="margin-top: 10px;">De bedste hilsner<br>{{$receiver_name}}</h5>
        </td>
    </tr>
</table>    