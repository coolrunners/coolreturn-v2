<?php 
$price_excl_tax = $price*0.80;
$price_incl_tax = $price;
?>

<table cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td>
            <a href="{{$website}}">
                <img src="{{$logo}}" />
            </a>
        </td>
    </tr>
    <tr>
        <td>
            <h1>Hi {{$sender_name}}</h1>
            <p>Thanks for creating your return label ({{$reference}}) through our return portal.</p>
            <p>If you haven't yet downloaded your label, we have attached it to this mail.</p>
            <p>Your packagenumber is: {{$package_number}} and after you've shipped it, you can track it on it's way to {{$receiver_name}} by visiting <a href="http://coolrunner.dk?trackandtrace={{$package_number}}">Coolrunner.dk</a></p>
        </td>
    </tr>
    <tr>
        <table width="100%" cellpadding="0" cellspacing="0" border="0">
            <thead>
              <th align="left">Net amount</th>
              <th align="left">Vat</th>
              <th align="left">Total amount</th>
            </thead>
            <tbody>
              <tr>
                <td>
                  {{$price_excl_tax}} kr.
                </td>
                <td>
                  {{$price_incl_tax-$price_excl_tax}} kr.
                </td>
                <td>
                  {{$price_incl_tax}} kr.
                </td>
              </tr>
            </tbody>
        </table>
    </tr>
    <tr>
        <td>
            <h5 class="closing-text" style="margin-top: 10px;">Kind regards<br>{{$receiver_name}}</h5>
        </td>
    </tr>
</table>    