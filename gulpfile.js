var elixir = require('laravel-elixir');

elixir(function(mix) {
    mix.less(['test.scss', 'errors.scss','success.scss']);
});
