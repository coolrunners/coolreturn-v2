<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;


class Routes extends Model
{

    public $timestamps = false;

    public $shipment = null;

    protected $fillable = [
        'name',
        'product',
        'title',
        'price',
        'description',
        'zone',
        'active',
        'customer_id',
        'logo',
        'url'
    ];

    protected $appends = ["receiver"];

    protected $hidden = array('id', 'customer_id');

    protected $sdk = null;


    public function __construct(array $attributes = [])
    {

        parent::__construct($attributes);
    }


    /**
     * disableDynamicAccessors
     *
     * @param array $accessors
     * @author Mikkel Nørgaard <min@coolrunner.dk>
     *
     */
    public function disableDynamicAccessors($accessors = [])
    {

        $this->setAppends($accessors);
    }


    /**
     * getReceiverAttribute
     *
     * @return mixed
     * @author Mikkel Nørgaard <min@coolrunner.dk>
     */
    public function getReceiverAttribute()
    {

        $this->sdk = \App\Http\Controllers\InternalAPI::loadInternal($this->customer_id);

        $customer = $this->sdk->get(env("API_URL") . "/v3/internal/{$this->customer_id}/customers")->jsonDecode();

        $customer->shipping->notification_email = $customer->shipping->notification_email ?? $customer->mail;

        foreach ($this->customReceiver as $key => $value) {
            $customer->shipping->{$key} = $value;
        }
        return $customer->shipping;


    }

    public function getPrivateAttribute()
    {
        return $this->settings->settings->private ?? 0;
    }

    public function getCustomReceiverAttribute()
    {
        return $this->settings->settings->receiver ?? [];
    }


    /**
     * settings
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     * @author Mikkel Nørgaard <min@coolrunner.dk>
     */
    public function settings()
    {

        return $this->hasOne('App\Route_settings', 'routes_id', 'id');
    }


    public function payments()
    {

        return $this->hasMany('App\Payment', 'route_id', 'id');
    }

    public function keys()
    {
        return $this->hasMany('App\Private_route_keys', 'route_id', 'id');
    }

    /**
     * createShipment
     *
     * @param Payment $order
     * @param         $sender
     *
     * @return \CoolRunnerSDK\Models\Shipments\ShipmentInfo|\Exception|false|\Throwable
     * @author Mikkel Nørgaard <min@coolrunner.dk>
     *
     */
    public function createShipment(Payment $order, $sender)
    {
        try {
            $sdk = \App\Http\Controllers\InternalAPI::loadInternal($this->customer_id);

            $shipment = new \App\Http\Controllers\API\Shipment();

            $product = $this->product();

            $shipment->sender = new \CoolRunnerSDK\Models\Properties\Person($sender);
            $shipment->receiver = (new Customer(session('view')->receiver))->createSender();

            $shipment->receiver->country = $this->receiver->country_id;
            $shipment->sender->country = $this->zone;

            $shipment->receiver->attention = "";
            $shipment->sender->attention = "";
            $shipment->receiver->street2 = "";
            $shipment->sender->street2 = "";

            $shipment->weight = $shipment->height = $shipment->width = $shipment->length = 5;
            // 34343
            $shipment->carrier = $product[0];
            $shipment->carrier_product = $product[1];
            $shipment->carrier_service = $product[2] ?? "";

            $shipment->servicepoint_id = 0;

            $shipment->return_price = $this->price;
            $shipment->reference = $sender['order_number'] ?? $order->order_id;

            $shipment->transaction_id = $order->transaction_id ?? '';

            $this->shipment = $shipment;

            $label = $sdk->createShipment($shipment);


            if (is_bool($label) AND $label == false) {
                if ($this->price <= 0) {
                    report(new \Exception($label));
                    throw new \Exception('Shipment Failed');

                } else {
                    $order->cancel();
                }
                throw new \Exception('Shipment Failed');

            } elseif (is_string($label)) {

                return $label;

            } elseif ($this->price > 0 AND $order->capture()->capture !== true) {
                $order->cancel();
                throw new \Exception('Payment Failed');
            }
            new Http\Controllers\Mail\Create($label, $order);
            return $label;
        } catch (\Throwable $e) {
            report($e);
            return $e;
        }


    }


    /**
     * product
     *
     * @return array
     * @author Mikkel Nørgaard <min@coolrunner.dk>
     */
    public function product()
    {
        try {
            $this->sdk = \App\Http\Controllers\InternalAPI::loadInternal($this->customer_id);
            $receiver = $this->receiver->country_id;
            $priceList = $this->sdk->getProducts($this->zone, $receiver);
            //$this->_findPrice($price);
            //die();


            switch (ucfirst($this->product)) {
                case 'Postnord':
                case 'Pdk' :
                    return [
                        'carrier' => 'pdk',
                        'carrier_product' => 'business',
                        'carrier_service' => 'return'
                    ];
                    break;

                default:
                    $product = $this->getPrice($priceList);
                    return [
                        'carrier' => $product[0] ?? null,
                        'carrier_product' => $product[1] ?? null,
                        'carrier_service' => $product[2] ?? null,
                    ];
            }

        } catch (\Throwable $e) {

        }


    }


    protected function getPrice($priceList)
    {

        $product = explode('_', $this->product, 3);

        if (count($product) > 1) {
            return $product;
        } else {
            return $this->findProductAmbiguous($priceList, reset($product));
        }

    }

    protected function findProductAmbiguous($price, $carrier)
    {

        $_product = [
            "carrier" => strtolower($carrier),
        ];

        foreach ($price[$carrier] as $key => $products) {

            if (strpos(strtolower($key), 'return') !== false) {
                $_product['carrier_product'] = strtolower($key);
                $_product['carrier_service'] = $products[0]->services[0]->code;

            } else {
                foreach ($products as $product) {
                    foreach ($product->services as $service_key => $service) {
                        if (strpos(strtolower($service->code), 'return') !== false) {
                            $_product['carrier_product'] = strtolower($key);
                            $_product['carrier_service'] = $service->code;
                        }
                    }
                }
            }

        }
        return array_values($_product);
    }

    public function getRouteAttribute() {
        return route('route', [
            "name" => $this->name,
            "zone" => $this->zone
        ]);
    }

}
