<?php


namespace App\Exceptions;


use App\Models\Error;
use App\Models\Payment;
use App\Models\Shipment;
use Throwable;

class ReturnsException extends \Exception
{
    protected $request;
    public function __construct(?array $request, $message = "", $code = 0, Throwable $previous = null)
    {
        $this->request = $request;
        parent::__construct($message, $code, $previous);
    }

    public function getRequest() {
        return $this->request;
    }



    public function send(?Shipment $shipment) {
        /** @var Error $error */
        $error = Error::createFromException($this, $shipment);
        return $error->route;
    }
}