<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateShipment extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name"  => "required",
            "sender_street1" => "required",
            "sender_zipcode" => "required",
            "sender_city" => "required",
            "sender_country" => "required",
            "sender_phone" => "required",
            "sender_email" => "required",
            "terms_conditions" => "required",
            "package_size" => "required"
        ];
    }
}
