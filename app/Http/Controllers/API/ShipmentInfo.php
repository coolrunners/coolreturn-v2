<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\InternalAPI as API;
use CoolRunnerSDK\Models\Error;
use CoolRunnerSDK\Models\Properties\Person;
use CoolRunnerSDK\Models\Shipments\ShipmentLinks;
use CoolRunnerSDK\Models\Shipments\ShipmentPrice;


class ShipmentInfo extends \CoolRunnerSDK\Models\Shipments\ShipmentInfo
{
    protected
        $package_number, $price, $_links, $product,
        $sender, $receiver,
        $length, $width, $height, $weight,
        $carrier, $carrier_product, $carrier_service,
        $reference, $description, $comment,
        $servicepoint_id, $label_format = 'LabelPrint',
        $labelless_code;

    protected function __construct($data = null)
    {
        if (!is_null($data) && is_array($data)) {

            foreach ($data as $key => $value) {
                if ($key === 'droppoint_id') {
                    $key = 'servicepoint_id';
                }

                $this->{$key} = $value;
            }

        }
        $this->price = new ShipmentPrice($this->price);
        $this->_links = new ShipmentLinks($this->_links);
        $this->sender = new Person($this->sender);
        $this->receiver = new Person($this->receiver);
    }

    public static function create($init_data) {

        return (new self($init_data))->getSelf();
    }

    public function getSelf() {
        if ($api = API::getInstance()) {
            return new self($api->get($this->_links->self)->jsonDecode(true));
        } else {
            Error::log(500, 'CoolRunner SDK must be instantiated before being able to pull data | ' . __FILE__);
        }
        return false;
    }

}