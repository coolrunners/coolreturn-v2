<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\InternalAPI as API;
use App\Http\Controllers\API\ShipmentInfo as ShipmentInfo;


class Shipment extends \CoolRunnerSDK\Models\Shipments\Shipment
{

    protected $return_price;

    protected $transaction_id;


    public function __construct ($data = null) {

        parent::__construct( $data );

        $this->_unset();

    }


    private function _unset () {

        unset( $this->length );
        unset( $this->width );
        unset( $this->height );

    }


    public function create () {

        try {


            $api = API::getInstance();
            if ( $this->validate() === true ) {


                $create_data = json_decode( json_encode( get_object_vars( $this ) ), true );

                $create_data[ 'droppoint_id' ] = $create_data[ 'servicepoint_id' ];


                $resp = $api->get( API::getBaseUrl() . 'return', 'POST', $create_data );


                if ( $resp->isOk() ) {
                    $resp = $api->get( $resp->jsonDecode( true )[ '_links' ][ 'self' ] );
                }
                else{

                    return trim($resp->toString(), '""');
                }

                $raw   = false;
                $assoc = false;
                if ( is_object( $api ) && get_class( $api ) === APILight::class ) {
                    $raw   = $api->isRaw();
                    $assoc = $api->isAssoc();
                }

                if ( $resp->isOk() ) {
                    if ( $raw || $assoc ) {
                        return $raw ? $resp->getData() : $resp->jsonDecode( $assoc );
                    }

                    return ShipmentInfo::create( $resp->jsonDecode( true ) );

                }

            }

        }
        catch ( \Throwable $e ) {
            report( $e );
            return $e;
        }
        report( new \Exception((function($validation) {
            if(is_array($validation)) {
                return json_encode($validation);
            } else {
                return (string)$validation;
            }
        })($this->validate())) );
        return false;
    }
}