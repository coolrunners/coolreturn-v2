<?php

namespace App\Http\Controllers;

//use App\Http\Controllers\API\Models\Shipment;
use CoolRunnerSDK\Models\Shipments\Shipment;
use App\Http\Controllers\API\ShipmentInfo as ShipmentInfo;
use CoolRunnerSDK\API as _API;


class InternalAPI extends _API
{

    protected static $_base_url = "https://api.coolrunner.dk/v3/internal/"; // TODO: change to LIVE
    protected static $cid;

    /**
     * loadInternal
     *
     * @author Mikkel Nørgaard <min@coolrunner.dk>
     *
     * @param      $customer_id
     * @param null $email
     * @param null $token
     * @param null $developer_id
     *
     * @return \CoolRunnerSDK\API
     */
    public static function loadInternal ($customer_id, $email = null, $token = null, $developer_id = null) {

        static::$_base_url = env( 'API_URL' ) . "/v3/internal/$customer_id/";
        if ( ! ( $email ) || ! ( $token ) ) {
            $email = env( 'API_AUTH_USER' );
            $token = env( 'API_AUTH_PW' );
        }

        if ( self::$_instance === false ) {
            self::$_instance = new self( base64_encode( "$email:$token") );
        }

        return self::$_instance;
    }

    //return self::load($email,$token,$developer_id);


    /**
     * @param Shipment $shipment
     *
     * @return ShipmentResponse|false
     */
    public function createShipment ($shipment) {

       // dd($shipment);

        if ( is_object( $shipment ) && get_class( $shipment ) === \App\Http\Controllers\API\Shipment::class ) {

            return $shipment->create();
        }

        return false;
    }

    /**
     * Get the current instance of CoolRunner API
     *
     * @return self|APILight|false CoolRunnerAPI if instance has been loaded, or false on failure
     */
    public static function getInstance() {
        return self::$_instance !== false ? self::$_instance : _API::getInstance();
    }


    /**
     * @return string
     */
    public static function getBaseUrl () {

        return static::$_base_url;
    }

    public function getShipmentLabel ($package_number) {

        $url = env('API_URL') . "/v3/return/$package_number/label";
        $resp = $this->get($url, 'GET');

        if ($resp->isOk()) {
            return $resp->getData();
        }
        return false;
    }

    public function getShipment($package_number) {
        $url = env('API_URL') . "/v3/return/$package_number";

        $resp = $this->get($url, 'GET');
        if ($resp->isOk()) {
            return ShipmentInfo::create($resp->jsonDecode(true));
        }
        return false;
    }



}
