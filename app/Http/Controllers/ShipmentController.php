<?php

namespace App\Http\Controllers;

use App\Exceptions\ReturnsException;
use App\Models\Shipment;
use App\Routes;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;


class ShipmentController extends Controller
{

    public function getPaymentAction(Request $request)
    {
        try {

            $route = session('view');
            $input = $request->all();

            $validation = $this->validateRequest($request, $route);

            /** Validate Shipment Request, if it fails return to previous page pre-filled and with errors */
            if ($validation->fails()) {
                return redirect()->back()
                    ->withInput($input)
                    ->withErrors($validation->getMessageBag());
            }
            // Create Shipment
            $shipment = Shipment::instance($input, $route);

            // Create Payment
            $payment = \App\Models\Payment::createFromShipment($shipment);

            $redirect = $payment->isPayable()
                ? $payment->payment_url
                : $shipment->redirect_url;

            return redirect($redirect);

        } catch (ReturnsException $e) {
            return $e->send($shipment ?? null);
        } catch (\Exception $e) {
            return redirect()->route('page-not-found');
        }
    }

    public function getProcessAction(Request $request, $shipment_id)
    {
        try {
            /** @var Shipment $shipment */
            $shipment = Shipment::with('payment')->where('id', $shipment_id)->whereHas('payment', function ($q) {
                $q->where('status', '<>', \App\Models\Payment::PAYMENT_STATUS_ERROR);
            })->firstOrFail();

            if ($shipment->privateRoute) {
                $shipment->privateRoute->setAsUsed();
            }

            $shipment = $shipment->process();
            return \Redirect::route('shipment-receipt', ["shipment_id_slug" => $shipment->slug()]);

        } catch (ModelNotFoundException $e) {
            return redirect()->route('page-not-found');
        } catch (ReturnsException $e) {
            return $e->send($shipment);
        } catch (\Throwable $e) {
            return (new ReturnsException(null, $e))->send($shipment);
        }
    }


    public function getReceiptAction(Request $request, $shipment_id_slug)
    {
        try {
            /** @var Shipment $shipment */
            $shipment = Shipment::findBySlugOrFail($shipment_id_slug);

            if (!$shipment->package_number) {
                throw new ReturnsException([], 'Package Number is not set');
            }

            \App::setLocale($shipment->sender["country"] == 'DK' ? 'da' : 'en_GB');

//        if ($shipment == false) throw new Exception('does not exist');
            return view('return.success', ['order' => $shipment->payment]);

            //App\Payment::where('package_number' ,$label->package_number)->first()
            //return view('return.success',["label" => $shipment]);

        } catch (ModelNotFoundException $e) {
            return redirect()->route('page-not-found');
        } catch (ReturnsException $e) {
            return $e->send($shipment);
        } catch (\Throwable $e) {
            return (new ReturnsException(null, $e->getMessage()))->send($shipment);
        }
    }

    protected function validateRequest(Request $request, ?Routes $route)
    {
        $rules = [
            "name" => "required",
            "street" => "required",
            "zip_code" => "required",
            "city" => "required",
            "phone" => "required",
            "email" => "required|email",
            "terms_conditions" => "required",
            "package_size" => "required",
        ];
        if ($route->settings) {
            foreach ($route->settings->settings ?? [] as $key => $val) {
                if (in_array($key, ['private', 'receiver'])) {
                    continue;
                }
                $rules[$key] = "required";
            }
        }

        return Validator::make($request->all(), $rules);
    }

    public function getPdfAction(Request $request, $shipment_slug)
    {
        /** @var Shipment $shipment */
        $shipment = Shipment::findBySlugOrFail($shipment_slug);

        $tempImage = tempnam(sys_get_temp_dir(), "pdf");
        $handle = fopen($tempImage, "w");
        fwrite($handle, $shipment->getPdf());
        fclose($handle);
        return response()->download($tempImage, $shipment->package_number . ".pdf");
    }

    public function getRouteAction(Request $request, $route_name, $route_zone = 'DK')
    {

        if ($route_zone == 'DK') {
            App::setLocale('da');
        }
        $_locale = 'en_GB';

        $route = \App\Routes::where('name', $route_name)->where('zone', $route_zone)->firstOrFail();

        if ($route->private >= 1) {
            return view('errors/404', ['data' => $route, "locale" => $_locale]);
        }

        return view('return', ['data' => $route, "locale" => $_locale]);

    }


    public function getPrivateRouteAction(Request $request, $private_route_key)
    {
        try {
            $private_route = \App\Private_route_keys::where('key', $private_route_key)->where('used', false)->firstOrFail();

            if ($private_route->route->zone == 'DK') {
                \App::setLocale('da');
            }
            $_locale = 'en_GB';
            session(['OTO_KEY' => $private_route]);

            return view('return', ['data' => $private_route->route, "locale" => $_locale]);
        } catch (\Throwable $e) {
            return redirect()->route('page-not-found');
        }
    }

    public function getErrorAction(Request $request, $error_slug)
    {
        $error = \App\Models\Error::findBySlug($error_slug);
        if ($error != null) {
            return response()->view('errors/error', ['error' => $error]);

        } else {
            return redirect()->route('page-not-found');
        }
    }

    public function get404Action()
    {
        return response()->view('errors/404');
    }


}
