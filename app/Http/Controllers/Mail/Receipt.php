<?php


namespace App\Http\Controllers\Mail;


use App\Models\Error;
use App\Models\Payment;
use SendGrid\Attachment;
use SendGrid\Content;
use SendGrid\Email;
use Sendgrid\Mail;


class Receipt
{


    public function __construct (Payment $payment) {
        try {
            $shipment = $payment->shipment;
            $receiver = (object)$shipment->receiver;
            $sender = (object)$shipment->sender;

            $from = new Email( "{$receiver->name} via CoolReturn", 'coolreturn@coolrunner.dk' );

            $subject = "Return label ({$shipment->package_number}) to {$receiver->name}";

            $to = new Email( $sender->name, $sender->email );
            $view = view( 'mail', [
                "sender_name"    => $sender->name,
                "receiver_name"  => $receiver->name,
                "reference"      => $shipment->reference,
                "package_number" => $shipment->package_number,
                "price"          => $shipment->price,
                "website"        => $payment->route->url,
                "logo"           => $payment->route->logo,
                "zone"           => $payment->route->zone ?? 'DK'
            ] );

            $content = new Content( "text/html", $view->render() );
            $mail     = new Mail( $from, $subject, $to, $content );

            $attachment = new Attachment();
            $attachment->setContent(base64_encode($shipment->getPdf()));
            $attachment->setType('application/pdf');
            $attachment->setFilename("{$shipment->package_number}.pdf");
            $attachment->setDisposition("attachment");
            $attachment->setContentId("Label");
            $mail->addAttachment($attachment);

            $apiKey   = getenv( 'SENDGRID_API_KEY' );
            $sg       = new \SendGrid($apiKey );
            $response = $sg->client->mail()->send()->post( $mail );

        }catch(\Throwable $e)
        {
          Error::createFromException($e, $payment);
        }

    }

    public static function send(Payment $payment)
    {
        return new static($payment);
    }
}