<?php

namespace App\Http\Controllers;

use Validator;


class SettingsController extends Controller
{

    public static function validation (Object $settings) {

        $inputs = [];

        foreach ( $settings  as $key => $value ) {

            switch ( $key ) {
                case 'att' :
                case 'order_number' :
                if ( (int)$value > 0 ) {
                        $inputs[ $key ] = [ "name" => $key, "type" => "text" ];
                    }
                    continue;
                    break;
                default :
                    continue;
                    break;
            }
        }

        return $inputs;
    }


}