<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Route_settings extends Model
{
    protected $fillable = ['css_style','settings'];


    public function getSettingsAttribute($value)
    {
        return json_decode($value);
    }
}


