<?php

namespace App\Models;

use App\Exceptions\ReturnsException;
use App\Lib\Curl;
use App\Routes;
use Illuminate\Database\Eloquent\Model;


/**
 * @property string transaction_id
 * @property string order_id
 * @property string payment_url
 * @property string process_url
 * @property string order_prefix
 * @property \App\Models\Shipment shipment
 */
class Payment extends Model
{

    const PAYMENT_STATUS_CREATED = 0;
    const PAYMENT_STATUS_CAPTURED = 1;
    const PAYMENT_STATUS_ERROR = 99;
    protected $fillable = ['order_id'];

    public static function createFromShipment(Shipment $shipment)
    {
        $customer_id = $shipment->route->customer_id;

        $base_url = Curl::getCoreBaseUrl();
        $query = http_build_query(["user_id" => $customer_id, "return_order_amount" => $shipment->price]);

        $url = "$base_url/returns/shipments?$query";
        $request = (object)(Curl::post($url, $shipment->toShipmentRequest()));

        if (Curl::isOk($request->http_code ?? null) && $request->response_data ?? false) {
            $response = (object)$request->response_data;

            $payment = new static();
            $payment->transaction_id = $response->transaction_id;
            $payment->order_id = "{$payment->order_prefix}{$customer_id}-{$response->transaction_id}";

            $payment->amount = $shipment->price;

            $payment->payment_url = $response->payment_url;
            $payment->process_url = $response->process_url;

            // Set Status based on absence of payment_url
            $payment->status = ($payment->payment_url)
                ? self::PAYMENT_STATUS_CREATED
                : self::PAYMENT_STATUS_CAPTURED;


            $payment->route()->associate($shipment->route);
            $payment->save();

            $shipment->payment()->associate($payment);
            $shipment->save();

            if (session('OTO_KEY')) {
                /** @var \App\Private_route_keys $private_route */
                $private_route = session('OTO_KEY');
                $private_route->shipment()->associate($shipment);
                $private_route->save();
            }
            return $payment;
        }
        throw new ReturnsException((array)$request, $request->status_message ?? 'Payment could not be created');
    }


    public function route()
    {
        return $this->belongsTo(Routes::class);
    }

    public function shipment()
    {
        return $this->hasOne(Shipment::class);
    }

    public function errors()
    {
        return $this->hasOne(Error::class, 'order_id', 'id');

    }


    public function getOrderPrefixAttribute()
    {
        return env('ORDER_PREFIX');
    }

    public function isPayable(): bool
    {
        return $this->amount && $this->payment_url;
    }


    public function getRouteUrl()
    {
        return $this->route->route;
    }

    public function getPackageNumberAttribute()
    {
        return $this->package_number ?: $this->shipment->package_number;
    }

}