<?php

namespace App\Models;


use App\Exceptions\ReturnsException;
use App\Http\Controllers\Mail\Create;
use App\Http\Controllers\Mail\Receipt;
use App\Lib\Curl;
use App\Private_route_keys;
use App\Routes;
use Balping\HashSlug\HasHashSlug;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed price
 * @property string carrier
 * @property string carrier_product
 * @property string carrier_service
 * @property mixed sender
 * @property mixed receiver
 *- @property mixed length
 * @property mixed width
 * @property mixed height
 * @property mixed redirect_url
 * @property \App\Models\Payment|null payment
 * @property mixed package_number
 * @property null|Private_route_keys privateRoute
 */
class Shipment extends Model
{

    use HasHashSlug;
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @author Mikkel Nørgaard
     */
    public function route()
    {
        return $this->belongsTo(Routes::class);
    }


    public function payment()
    {
        return $this->belongsTo(Payment::class);
    }

    /**
     * Shipment constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @param $request
     * @param \App\Routes $route
     * @return \App\Models\Shipment
     * @throws \App\Exceptions\ReturnsException
     * @author Mikkel Nørgaard
     */
    public static function instance($request, \App\Routes $route)
    {
        try {
            $shipment = new static();

            $product = ($route->product());

            $shipment->route()->associate($route);
            $shipment->price = $route->price;

            $shipment->carrier = $product['carrier'];
            $shipment->carrier_product = $product['carrier_product'];
            $shipment->carrier_service = $product['carrier_service'];


            foreach (self::extractSenderFromRequest(array_merge($request,
                ['country' => $route->zone])) as $key => $value) {
                $shipment->{"sender_$key"} = $value;
            }

            foreach (self::extractReceiverFromRequest((array)$route->receiver) as $key => $value) {
                $shipment->{"receiver_$key"} = $value;
            }

            $shipment->save();
            // TODO : ADD Reference

            return $shipment;
        } catch (\Throwable $e) {
          throw new ReturnsException(null, $e->getMessage());
        }
    }

    /**
     * @return array
     * @author Mikkel Nørgaard
     */
    public function toShipmentRequest()
    {
        return [
            "carrier" => $this->carrier,
            "carrier_product" => $this->carrier_product,
            "carrier_service" => $this->carrier_service,
            "sender" => $this->sender,
            "receiver" => $this->receiver,
            "length" => $this->length,
            "width" => $this->width,
            "height" => $this->height,
            "weight" => $this->weight,
            "redirect_url" => $this->redirect_url
        ];
    }

    /**
     * @param array $request
     * @return array
     * @author Mikkel Nørgaard
     */
    protected static function extractSenderFromRequest(array $request)
    {

        return [
            "name" => $request["name"] ?? null,
            "attention" => $request["attention"] ?? null,
            "street1" => $request["street"] ?? null,
            "zip_code" => $request["zip_code"] ?? null,
            "city" => $request["city"] ?? null,
            "country" => $request["country"] ?? null,
            "phone" => $request["phone"] ?? null,
            "mail" => $request["email"] ?? null,

        ];
    }

    public function process()
    {
        try {
            if ($this->package_number) {
                return $this;
            }

            $request = Curl::get("{$this->payment->process_url}?save_to_magento=1");

            if (Curl::isException($request)) {
                throw new ReturnsException(null, $request['message'] ?? null);
            } else {
                if (Curl::requestIsOk($request)) {
                    $this->package_number = $request['response_data']['package_number'];
                    $this->labelless_code = $request['response_data']['labelless_code'];
                    $this->payment->status = Payment::PAYMENT_STATUS_CAPTURED;
                    $this->payment->save();
                    $this->save();

                    // Send receipt via Mail
                    Receipt::send($this->payment);

                    return $this;
                }
            }

        } catch (\Throwable $e) {
            throw new ReturnsException($request ?? null, $e->getMessage());
        }
        throw new ReturnsException($request ?? null, 'Request Failed');
    }

    public function getPdf()
    {
        $base_url = Curl::getCoreBaseUrl();
        $query = http_build_query(["user_id" => $this->route->customer_id]);
        return file_get_contents("{$base_url}public/shipments/{$this->package_number}/label?$query");
    }

    /**
     * @param array $request
     * @return array
     * @author Mikkel Nørgaard
     */
    protected static function extractReceiverFromRequest(array $request)
    {

        return [
            "name" => $request["company"] ?: $request["firstname"] . " " . $request["lastname"],
            "attention" => $request["attention"] ?? null,
            "street1" => $request["street"] ?? null,
            "zip_code" => $request["postcode"] ?? null,
            "city" => $request["city"] ?? null,
            "country" => $request["country_id"] ?? null,
            "phone" => $request["telephone"] ?? null,
            "mail" => $request["notification_email"] ?? null,

        ];
    }

    /**
     * @return int
     * @author Mikkel Nørgaard
     */
    public function getWeightAttribute()
    {
        return 5;
    }

    /**
     * @return int
     * @author Mikkel Nørgaard
     */
    public function getLengthAttribute()
    {
        return 5;

    }

    /**
     * @return int
     * @author Mikkel Nørgaard
     */
    public function getHeightAttribute()
    {
        return 5;

    }

    /**
     * @return int
     * @author Mikkel Nørgaard
     */
    public function getWidthAttribute()
    {
        return 5;
    }

    /**
     * @return array
     * @author Mikkel Nørgaard
     */
    public function getSenderAttribute()
    {
        return
            [
                "name" => $this->sender_name,
                "attention" => $this->sender_attention,
                "street1" => $this->sender_street1,
                "zip_code" => $this->sender_zip_code,
                "city" => $this->sender_city,
                "country" => $this->sender_country,
                "phone" => $this->sender_phone,
                "email" => $this->sender_mail,
            ];
    }

    /**
     * @return array
     * @author Mikkel Nørgaard
     */
    public function getReceiverAttribute()
    {
        return
            [
                "name" => $this->receiver_name,
                "attention" => $this->receiver_attention,
                "street1" => $this->receiver_street1,
                "zip_code" => $this->receiver_zip_code,
                "city" => $this->receiver_city,
                "country" => $this->receiver_country,
                "phone" => $this->receiver_phone,
                "email" => $this->receiver_mail,
            ];
    }

    public function getRedirectUrlAttribute()
    {
        return route('shipment-process', ['shipment_id' => $this->id]);
    }

    public function privateRoute()
    {
        return $this->hasOne(Private_route_keys::class);
    }

}