<?php

namespace App;

use App\Models\Shipment;
use Illuminate\Database\Eloquent\Model;

/**
 * @property bool used
 */
class Private_route_keys extends Model
{
    protected $fillable = ['route_id','key'];
    protected $casts = [
        "used" => "boolean"
    ];

    public function route () {

        return $this->belongsTo( Routes::class, 'route_id','id' );
    }
    public function shipment () {

        return $this->belongsTo( Shipment::class);
    }

    public function setAsUsed()
    {
        $this->used = true;
        $this->save();
    }
}
