<?php

namespace App\Providers;

use App\Errors;
use Carbon\Carbon;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;


class AppServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot () {


        Blade::directive( 'date', function($expression) {

            return "<?php echo ($expression)->format('d.m.Y'); ?>";
        } );

        Blade::directive( 'firstname', function($expression) {
            return "<?php echo explode(' ',trim($expression))[0]; ?>";
        } );



    }


    /**
     * Register any application services.
     *
     * @return void
     */
    public function register () {
        //
    }
}
