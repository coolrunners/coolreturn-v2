<?php

namespace App\Lib;


class Curl
{

    const CURL_METHOD_GET = 'GET';
    const CURL_METHOD_POST = 'POST';
    const CURL_METHOD_PUT = 'PUT';
    const CURL_METHOD_DELETE = 'DELETE';

    const CONTENT_TYPE_JSON = 'Content-Type: application/json';

    public static function getCoreBaseUrl()
    {
        return env('API_CORE_URL');
    }

    public static function get($url, ?array $headers = [self::CONTENT_TYPE_JSON])
    {
        return self::request($url, self::CURL_METHOD_GET, null, $headers);
    }

    public static function post($url, $body = [], ?array $headers = [self::CONTENT_TYPE_JSON])
    {
        return self::request($url, self::CURL_METHOD_POST, $body, $headers);
    }

    public static function put($url, $body = [], ?array $headers = [self::CONTENT_TYPE_JSON])
    {
        return self::request($url, self::CURL_METHOD_PUT, $body, $headers);
    }

    public static function delete($url, ?array $headers = [self::CONTENT_TYPE_JSON])
    {
        return self::request($url, self::CURL_METHOD_DELETE, null, $headers);
    }

    /**
     * @param $url
     * @param string $method
     * @param array|null $body
     * @param array|null $headers
     * @param bool $encode
     * @return array|bool|mixed|object|\SimpleXMLElement|string
     * @author Mikkel Nørgaard
     */
    public static function request(
        $url,
        $method = self::CURL_METHOD_GET,
        $body = [],
        ?array $headers = [],
        $encode = true
    ) {
        if (self::encodeJson($headers, $body) && $encode) {
            $body = json_encode($body);
        }

        $request = self::_($url, [
            "header" => $headers,
            "method" => $method,
            "body" => $body,
            "header" => $headers
        ]);

        if (self::decodeJson($headers, $request) && $encode) {
            $request = json_decode($request, true);
        }

        return $request;

    }

    protected static function encodeJson(?array $headers, $body): bool
    {
        return in_array(self::CONTENT_TYPE_JSON, $headers) && is_array($body);
    }

    protected static function decodeJson(?array $headers, $body): bool
    {
        return in_array(self::CONTENT_TYPE_JSON, $headers) && !is_array($body);
    }

    public static function _($url, $options = array())
    {
        // predifined options
        // Curl options ------------------
        $curlOptions = array(
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_URL => $url,
            CURLOPT_TIMEOUT => 20,
            CURLOPT_RETURNTRANSFER => true
        );
        // benchmark settings
        $benchmark = array(
            "enabled" => true,
            "carrier" => "",
            "request" => ""
        );

        // misc settings
        $slack = false;
        $response_type = "";
        $debug = false;
        $debug_message = false;
        //  -------------------------------
        // assign/reassign options
        foreach ($options as $option => $value) {
            switch ($option) {
                case 'header':
                    $curlOptions[CURLOPT_HTTPHEADER] = $value;
                    break;
                case 'body' :
                    $curlOptions[CURLOPT_POSTFIELDS] = $value;
                    break;
                case 'slack' :
                    $slack = $value;
                    break;
                case 'curl_options' :
                    foreach ($value as $setting => $data) {
                        $curlOptions[$setting] = $data;
                    }
                    break;
                case 'benchmark' :
                    foreach ($value as $setting => $data) {
                        if (isset($benchmark[$setting])) {
                            $benchmark[$setting] = $data;
                        }
                    }
                    break;
                case 'response_type' :
                    $response_type = $value;
                    break;
                case 'method' :
                    $curlOptions[CURLOPT_CUSTOMREQUEST] = $value;
                    break;
                case 'debug' :
                    $debug = is_bool($value) ? $value : false;
                    break;
                case 'debug_message' :
                    $debug_message = is_bool($value) ? $value : false;
                    break;
                default :
                    throw new Error("curl client does not support the use of " . $option);
            }
        }
        $ch = curl_init();
        curl_setopt_array($ch, $curlOptions);

        $result = curl_exec($ch);
        $info = curl_getinfo($ch);

        curl_close($ch);

        if ($debug) {
            $result = ['response' => $result, 'info' => $info];

        }

        switch (strtoupper($response_type)) {
            case 'JSON' :
                return ($debug) ? json_decode($result['response'], true) : json_decode($result, true);
                break;
            case 'JSON_OBJECT' :
                return ($debug) ? json_decode($result['response']) : json_decode($result);
                break;
            case 'XML' :
                return ($debug) ? simplexml_load_string($result['response']) : simplexml_load_string($result);
                break;
            default:
                return $result;
        }

    }

    public static function isOk(?int $http_code)
    {
        return ($http_code >= 200 && $http_code < 300);
    }

    public static function requestIsOk($request)
    {
        $http_code = ($request["http_code"] ?? ($request['response_code'] ?? 0));
        return static::isOk($http_code);
    }

    public static function isException($request) {
        return ((is_object($request) && $request->exception ?? false) || (is_array($request) && isset($request['exception'])));
    }
}