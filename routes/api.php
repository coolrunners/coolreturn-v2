<?php

use Illuminate\Http\Request;
Route::get('/routes/{oid}', function($cid)
{
    $routes =  \App\Routes::where('customer_id',$cid)->with('settings')->get();
    //$routes->disableDynamicAccessors();

    //$routes['product'] = $routes->product();

    return response($routes->toJson(), 200)
        ->header('Content-Type', 'application/json');
    //return response(["status" => "ok","message" => "", 'body' =>$routes], 200);
});

Route::get('routes/keys/{cid}', function($cid) {
    try {
    $routes = \App\Routes::where('customer_id',$cid)->with('keys')->get();

    foreach ($routes as $key => $route) {
        //unset($routes[$key]['receiver']);
        if(count($route->keys) < 1) unset($routes[$key]);

    }


    return response($routes->toJson(), 200)
        ->header('Content-Type', 'application/json');
    }catch (Throwable $e) {
        dump($e->getMessage());
    }
});

Route::get('routes/keys/{cid}/{route}', function($cid, $route_name) {
    $routes = \App\Routes::where('customer_id',$cid)->where('name',$route_name)->firstOrFail();

    return response($routes->keys, 200)
        ->header('Content-Type', 'application/json');
});



Route::get('/routes/{cid}/{zone}', function($cid,$zone)
{
    $routes =  \App\Routes::where('customer_id',$cid)->where('zone',$zone)->first();
    //$routes->disableDynamicAccessors();

    //$routes['product'] = $routes->product();
    return response($routes->toJson(), 200)
        ->header('Content-Type', 'application/json');

    //return response(["status" => "ok","message" => "", 'body' =>$routes], 200);
});


/*
 * requires following data from request :
 * name, title, description, zone, product.
 *
 * if settings are requested, then following are required in array:
 *
 */
Route::post('/routes/{oid}', function(Request $request, $cid)
{
    $data = $request->all();



    $validator = Validator::make($request->all(), [
        'name' => 'required|regex:/^[^-\s]+$/i|max:255',
        'product' => 'required|alpha_dash|max:255',
        'zone' => 'required|max:2'
    ]);


    if ($validator->fails()) {
        return response()->json($validator->errors(), 422);
    }


    if( $route = \App\Routes::where('name',$data['name'])->where('zone',$data['zone'])->first()) {
        if($route->customer_id = $cid) {
          $route->update($data);
          try {
              $route->settings()->update($data['settings']);
          } catch(Exception $e) {
              return response($e->getMessage(), 200)
                  ->header('Content-Type', 'application/json');
          }
        }
    } else {
        $data['customer_id'] = $cid;
        $route = \App\Routes::create($data);
        $route->settings()->create();

        if(isset($data['settings'])) {

            $route->settings()->update($data['settings']);

        }
    }

    return response($route->toJson(), 200)
        ->header('Content-Type', 'application/json');

});

Route::post('key/{oid}/{route_name}/{zone}', function(Request $request, $cid, $route_name,$zone) {
    try {
    $route =  \App\Routes::where('customer_id',$cid)->where('name',$route_name)->where('zone',$zone)->firstOrFail();

    if($route->private < 1) {
        return response("nope", 403)
            ->header('Content-Type', 'application/json');
    }

        $unique_salt = (int)((\Carbon\Carbon::now()->timestamp / (microtime(true) * 100))/$route->id);

        $hash =  md5("{$cid}_{$route_name}_{$zone}".microtime(true));
        return $route->keys()->create([
            "route_id" => $route->id,
            "key" => srand($unique_salt) . substr($hash,strlen($unique_salt))
        ]);
    }catch (Throwable $e)
    {
        dd($e->getMessage());
    }
});



Route::get('orders/{oid}', function($cid) {
    $orders = [];
    $routes =  \App\Routes::where('customer_id',$cid)->get();
    foreach($routes as $route)
    {
        foreach($route->payments as $payment) {
            $orders[] = $payment->toArray();
        };

    }
    //$routes->disableDynamicAccessors();

    return response($routes);
});

/**
 * uid is a md5 encryption of name and zone
 */
Route::delete('routes/{uid}/{oid}', function(Request $request,$uid, $cid) {

    $response = \App\Routes::where(DB::raw('md5(concat(name,zone))'), $uid)->where('customer_id',$cid)->delete();

    if($response == 1) {
        return response(null, 200);
    } else  {
        return response(null, 410);
    }

});