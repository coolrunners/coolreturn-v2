<?php

Route::group(['domain' => 'coolreturn.coolrunner.dk'], function () {

    // All my subdomain routes here ...

    // And then
    Route::get('{slug}', function ($slug) {
        //TODO: redirect this to the main domain + the slug that came in -- somehow :confused:
        return redirect(Config::get('app.url') . $slug);
    });

});

Route::get('/404', array('as' => '404', function () {
    return response()->view('errors/404');
}));


Route::get('/', function () {
    return response()->view('errors/404');
});


Route::get('/s/{key}', function ($key) {
    $route = \App\Private_route_keys::where('key', $key)->firstOrFail();

    if ($route->route->zone == 'DK') App::setLocale('da-DK');
    $_locale = 'en_GB';


    if ($route->used >= 1) {
        return view('errors/410', ['data' => $route, "locale" => $_locale]);
    }
    session(['OTO_KEY' => $route]);

    return view('return', ['data' => $route->route, "locale" => $_locale]);

});

Route::get('/coolrunner', function () {
    dd('testing');
});

Route::group(['prefix' => "request", 'middleware' => ['web']], function () {

    Route::post('/', 'ShipmentController@payment')->middleware();

});
Route::get('request/create/{order_id}', 'ShipmentController@Capture');

// get base64 pdf and render from tmpdir
Route::get('request/pdf/{package_number}', function ($package_number) {

    $shipment = App\Http\Controllers\InternalAPI::loadInternal(null)->getShipment($package_number);
    $tempImage = tempnam(sys_get_temp_dir(), "pdf");

    $handle = fopen($tempImage, "w");
    fwrite($handle, $shipment->getLabel());
    fclose($handle);
    return response()->download($tempImage, $shipment->package_number . ".pdf");

});


Route::get('/success/{package_number}', function ($package_number) {
    try {
        $shipment = App\Http\Controllers\InternalAPI::loadInternal(null)->getShipment($package_number);

        App::setLocale($shipment->sender->country == 'DK' ? 'dk' : 'en_GB');
        if ($shipment == false) throw new Exception('does not exist');
        return view('return.success', ['order' => \App\Payment::where('package_number', $package_number)->firstOrFail()]);

        //App\Payment::where('package_number' ,$label->package_number)->first()
        //return view('return.success',["label" => $shipment]);
    } catch (Throwable $e) {
        return response()->view('errors/404');
    }
});


Route::get('/error', function () {

    return response()->view('errors/validation');
});


Route::get('/error/{unique_id}', function ($uid) {
    $error = \App\Errors::where('unique_id', $uid)->first();
    if ($error != null) {
        return response()->view('errors/error', ['error' => $error]);

    } else {
        return \Illuminate\Support\Facades\Redirect::route('404');
    }
});

Route::get("/create/{hash}/{zone}", function ($hash, $zone) {

    $customer_id = str_replace(md5($zone . 'return'), '', $hash);

    return response()->view('create/default', ['data' => $customer_id]);

    //return $data;
});

Route::get("/{name}/{zone?}", function ($name, $zone = "") {

    if (empty($zone)) return redirect("/$name/DK");

    App::setLocale(\Illuminate\Support\Facades\Lang::get("translations.$zone"));

//    $_locale = explode(',',str_replace('-','_', Request::server('HTTP_ACCEPT_LANGUAGE')))[0] ?? 'en_GB';
    $_locale = 'en_GB';
    $route = \App\Routes::where('name', $name)->where('zone', $zone)->firstOrFail();

    if ($route->private >= 1) {
        return view('errors/404', ['data' => $route, "locale" => $_locale]);
    }

    return view('return', ['data' => $route, "locale" => $_locale]);

});

