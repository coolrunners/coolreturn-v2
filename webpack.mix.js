let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent InternalAPI for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .sass('resources/assets/sass/test.scss', 'public/css')
    .sass('resources/assets/sass/errors.scss', 'public/css')
    .sass('resources/assets/sass/success.scss','public/css')
    .styles([
        'public/css/styles.css',
        'public/css/test.css'
    ], 'public/css/concat.css')
    .version();

